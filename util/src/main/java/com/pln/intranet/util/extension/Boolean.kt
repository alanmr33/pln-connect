package com.pln.intranet.util.extension

fun Boolean?.orFalse(): Boolean = this ?: false
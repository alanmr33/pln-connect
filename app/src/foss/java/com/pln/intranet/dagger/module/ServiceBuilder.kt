package com.pln.intranet.dagger.module

import com.pln.intranet.chatroom.di.MessageServiceProvider
import com.pln.intranet.chatroom.service.MessageService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class ServiceBuilder {
    @ContributesAndroidInjector(modules = [MessageServiceProvider::class])
    abstract fun bindMessageService(): MessageService
}

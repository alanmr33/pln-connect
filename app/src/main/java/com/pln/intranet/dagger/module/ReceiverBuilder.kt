package com.pln.intranet.dagger.module

import com.pln.intranet.push.DeleteReceiver
import com.pln.intranet.push.DirectReplyReceiver
import com.pln.intranet.push.DirectReplyReceiverProvider
import com.pln.intranet.push.di.DeleteReceiverProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ReceiverBuilder {

    @ContributesAndroidInjector(modules = [DeleteReceiverProvider::class])
    abstract fun bindDeleteReceiver(): DeleteReceiver

    @ContributesAndroidInjector(modules = [DirectReplyReceiverProvider::class])
    abstract fun bindDirectReplyReceiver(): DirectReplyReceiver
}
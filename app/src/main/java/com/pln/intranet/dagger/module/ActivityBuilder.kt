package com.pln.intranet.dagger.module

import com.pln.intranet.about.di.AboutFragmentProvider
import com.pln.intranet.authentication.di.AuthenticationModule
import com.pln.intranet.authentication.login.di.LoginFragmentProvider
import com.pln.intranet.authentication.loginoptions.di.LoginOptionsFragmentProvider
import com.pln.intranet.authentication.onboarding.di.OnBoardingFragmentProvider
import com.pln.intranet.authentication.registerusername.di.RegisterUsernameFragmentProvider
import com.pln.intranet.authentication.resetpassword.di.ResetPasswordFragmentProvider
import com.pln.intranet.authentication.server.di.ServerFragmentProvider
import com.pln.intranet.authentication.signup.di.SignupFragmentProvider
import com.pln.intranet.authentication.twofactor.di.TwoFAFragmentProvider
import com.pln.intranet.authentication.ui.AuthenticationActivity
import com.pln.intranet.chatdetails.di.ChatDetailsFragmentProvider
import com.pln.intranet.chatdetails.di.ChatDetailsModule
import com.pln.intranet.chatdetails.ui.ChatDetailsActivity
import com.pln.intranet.chatinformation.di.MessageInfoFragmentProvider
import com.pln.intranet.chatinformation.ui.MessageInfoActivity
import com.pln.intranet.chatroom.di.ChatRoomFragmentProvider
import com.pln.intranet.chatroom.di.ChatRoomModule
import com.pln.intranet.chatroom.ui.ChatRoomActivity
import com.pln.intranet.chatrooms.di.ChatRoomsFragmentProvider
import com.pln.intranet.createchannel.di.CreateChannelProvider
import com.pln.intranet.dagger.scope.PerActivity
import com.pln.intranet.draw.main.di.DrawModule
import com.pln.intranet.draw.main.ui.DrawingActivity
import com.pln.intranet.favoritemessages.di.FavoriteMessagesFragmentProvider
import com.pln.intranet.files.di.FilesFragmentProvider
import com.pln.intranet.main.di.MainModule
import com.pln.intranet.main.ui.MainActivity
import com.pln.intranet.members.di.MembersFragmentProvider
import com.pln.intranet.mentions.di.MentionsFragmentProvider
import com.pln.intranet.pinnedmessages.di.PinnedMessagesFragmentProvider
import com.pln.intranet.preferences.di.PreferencesFragmentProvider
import com.pln.intranet.profile.di.ProfileFragmentProvider
import com.pln.intranet.server.di.ChangeServerModule
import com.pln.intranet.server.ui.ChangeServerActivity
import com.pln.intranet.settings.di.SettingsFragmentProvider
import com.pln.intranet.settings.password.di.PasswordFragmentProvider
import com.pln.intranet.settings.password.ui.PasswordActivity
import com.pln.intranet.userdetails.di.UserDetailsModule
import com.pln.intranet.userdetails.ui.UserDetailsActivity
import com.pln.intranet.webview.adminpanel.di.AdminPanelWebViewFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @PerActivity
    @ContributesAndroidInjector(
        modules = [AuthenticationModule::class,
            OnBoardingFragmentProvider::class,
            ServerFragmentProvider::class,
            LoginOptionsFragmentProvider::class,
            LoginFragmentProvider::class,
            RegisterUsernameFragmentProvider::class,
            ResetPasswordFragmentProvider::class,
            SignupFragmentProvider::class,
            TwoFAFragmentProvider::class
        ]
    )
    abstract fun bindAuthenticationActivity(): AuthenticationActivity

    @PerActivity
    @ContributesAndroidInjector(
        modules = [MainModule::class,
            ChatRoomsFragmentProvider::class,
            CreateChannelProvider::class,
            ProfileFragmentProvider::class,
            SettingsFragmentProvider::class,
            AboutFragmentProvider::class,
            PreferencesFragmentProvider::class,
            AdminPanelWebViewFragmentProvider::class
        ]
    )
    abstract fun bindMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(
        modules = [
            ChatRoomModule::class,
            ChatRoomFragmentProvider::class
        ]
    )
    abstract fun bindChatRoomActivity(): ChatRoomActivity

    @PerActivity
    @ContributesAndroidInjector(
        modules = [
            ChatDetailsModule::class,
            ChatDetailsFragmentProvider::class,
            MembersFragmentProvider::class,
            MentionsFragmentProvider::class,
            PinnedMessagesFragmentProvider::class,
            FavoriteMessagesFragmentProvider::class,
            FilesFragmentProvider::class
        ]
    )
    abstract fun bindChatDetailsActivity(): ChatDetailsActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [PasswordFragmentProvider::class])
    abstract fun bindPasswordActivity(): PasswordActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ChangeServerModule::class])
    abstract fun bindChangeServerActivity(): ChangeServerActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [MessageInfoFragmentProvider::class])
    abstract fun bindMessageInfoActiviy(): MessageInfoActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [DrawModule::class])
    abstract fun bindDrawingActivity(): DrawingActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [UserDetailsModule::class])
    abstract fun bindUserDetailsActivity(): UserDetailsActivity
}

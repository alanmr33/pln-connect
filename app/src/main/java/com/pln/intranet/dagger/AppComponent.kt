package com.pln.intranet.dagger

import android.app.Application
import com.pln.intranet.app.RocketChatApplication
import com.pln.intranet.chatroom.service.MessageService
import com.pln.intranet.dagger.module.ActivityBuilder
import com.pln.intranet.dagger.module.AndroidWorkerInjectionModule
import com.pln.intranet.dagger.module.AppModule
import com.pln.intranet.dagger.module.ReceiverBuilder
import com.pln.intranet.dagger.module.ServiceBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    AppModule::class, ActivityBuilder::class, ServiceBuilder::class, ReceiverBuilder::class,
    AndroidWorkerInjectionModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: RocketChatApplication)

    fun inject(service: MessageService)
}

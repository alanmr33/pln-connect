package com.pln.intranet.pinnedmessages.presentation

import com.pln.intranet.chatroom.uimodel.BaseUiModel
import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView

interface PinnedMessagesView : MessageView, LoadingView {

    /**
     * Show list of pinned messages for the current room.
     *
     * @param pinnedMessages The list of pinned messages.
     */
    fun showPinnedMessages(pinnedMessages: List<BaseUiModel<*>>)
}
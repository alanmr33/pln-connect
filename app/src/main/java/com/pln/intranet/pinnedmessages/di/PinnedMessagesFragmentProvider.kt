package com.pln.intranet.pinnedmessages.di

import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.pinnedmessages.ui.PinnedMessagesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PinnedMessagesFragmentProvider {

    @ContributesAndroidInjector(modules = [PinnedMessagesFragmentModule::class])
    @PerFragment
    abstract fun providePinnedMessageFragment(): PinnedMessagesFragment
}
package com.pln.intranet.userdetails.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.dagger.scope.PerActivity
import com.pln.intranet.userdetails.presentation.UserDetailsView
import com.pln.intranet.userdetails.ui.UserDetailsActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.experimental.Job

@Module
class UserDetailsModule {

    @Provides
    @PerActivity
    fun provideJob() = Job()

    @Provides
    @PerActivity
    fun provideUserDetailsView(activity: UserDetailsActivity): UserDetailsView = activity

    @Provides
    @PerActivity
    fun provideLifecycleOwner(activity: UserDetailsActivity): LifecycleOwner = activity

    @Provides
    @PerActivity
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}

package com.pln.intranet.authentication.registerusername.presentation

import com.pln.intranet.analytics.AnalyticsManager
import com.pln.intranet.analytics.event.AuthenticationEvent
import com.pln.intranet.authentication.presentation.AuthenticationNavigator
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.server.domain.GetConnectingServerInteractor
import com.pln.intranet.server.domain.GetSettingsInteractor
import com.pln.intranet.server.domain.PublicSettings
import com.pln.intranet.server.domain.SaveAccountInteractor
import com.pln.intranet.server.domain.SaveCurrentServerInteractor
import com.pln.intranet.server.domain.TokenRepository
import com.pln.intranet.server.domain.favicon
import com.pln.intranet.server.domain.model.Account
import com.pln.intranet.server.domain.wideTile
import com.pln.intranet.server.infraestructure.RocketChatClientFactory
import com.pln.intranet.util.extension.launchUI
import com.pln.intranet.util.extensions.avatarUrl
import com.pln.intranet.util.extensions.serverLogoUrl
import com.pln.intranet.util.retryIO
import chat.rocket.common.RocketChatException
import chat.rocket.common.model.Token
import chat.rocket.common.util.ifNull
import chat.rocket.core.RocketChatClient
import chat.rocket.core.internal.rest.updateOwnBasicInformation
import javax.inject.Inject

class RegisterUsernamePresenter @Inject constructor(
    private val view: RegisterUsernameView,
    private val strategy: CancelStrategy,
    private val navigator: AuthenticationNavigator,
    private val tokenRepository: TokenRepository,
    private val saveAccountInteractor: SaveAccountInteractor,
    private val analyticsManager: AnalyticsManager,
    private val saveCurrentServer: SaveCurrentServerInteractor,
    val serverInteractor: GetConnectingServerInteractor,
    val factory: RocketChatClientFactory,
    val settingsInteractor: GetSettingsInteractor
) {
    private val currentServer = serverInteractor.get()!!
    private val client: RocketChatClient = factory.create(currentServer)
    private var settings: PublicSettings = settingsInteractor.get(serverInteractor.get()!!)

    fun registerUsername(username: String, userId: String, authToken: String) {
        launchUI(strategy) {
            view.showLoading()
            try {
                val me = retryIO("updateOwnBasicInformation(username = $username)") {
                    client.updateOwnBasicInformation(username = username)
                }
                val registeredUsername = me.username
                if (registeredUsername != null) {
                    saveAccount(registeredUsername)
                    saveCurrentServer.save(currentServer)
                    tokenRepository.save(currentServer, Token(userId, authToken))
                    analyticsManager.logSignUp(
                        AuthenticationEvent.AuthenticationWithOauth,
                        true
                    )
                    navigator.toChatList()
                }
            } catch (exception: RocketChatException) {
                analyticsManager.logSignUp(AuthenticationEvent.AuthenticationWithOauth, false)
                exception.message?.let {
                    view.showMessage(it)
                }.ifNull {
                    view.showGenericErrorMessage()
                }
            } finally {
                view.hideLoading()
            }
        }
    }

    private suspend fun saveAccount(username: String) {
        val icon = settings.favicon()?.let {
            currentServer.serverLogoUrl(it)
        }
        val logo = settings.wideTile()?.let {
            currentServer.serverLogoUrl(it)
        }
        val thumb = currentServer.avatarUrl(username)
        val account = Account(currentServer, icon, logo, username, thumb)
        saveAccountInteractor.save(account)
    }
}
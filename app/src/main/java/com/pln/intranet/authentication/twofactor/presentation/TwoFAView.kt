package com.pln.intranet.authentication.twofactor.presentation

import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView

interface TwoFAView : LoadingView, MessageView {

    /**
     * Enables the button to set the username if the user entered at least one character.
     */
    fun enableButtonConfirm()

    /**
     * Disables the button to set the username when there is no character entered by the user.
     */
    fun disableButtonConfirm()

    /**
     * Alerts the user about an invalid inputted Two Factor Authentication code.
     */
    fun alertInvalidTwoFactorAuthenticationCode()
}
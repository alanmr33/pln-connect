package com.pln.intranet.authentication.resetpassword.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.resetpassword.presentation.ResetPasswordView
import com.pln.intranet.authentication.resetpassword.ui.ResetPasswordFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class ResetPasswordFragmentModule {

    @Provides
    @PerFragment
    fun resetPasswordView(frag: ResetPasswordFragment): ResetPasswordView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ResetPasswordFragment): LifecycleOwner = frag
}
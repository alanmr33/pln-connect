package com.pln.intranet.authentication.loginoptions.di

import com.pln.intranet.authentication.loginoptions.ui.LoginOptionsFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class LoginOptionsFragmentProvider {

    @ContributesAndroidInjector(modules = [LoginOptionsFragmentModule::class])
    @PerFragment
    abstract fun providesLoginOptionFragment(): LoginOptionsFragment
}
package com.pln.intranet.authentication.registerusername.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.registerusername.presentation.RegisterUsernameView
import com.pln.intranet.authentication.registerusername.ui.RegisterUsernameFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class RegisterUsernameFragmentModule {

    @Provides
    @PerFragment
    fun registerUsernameView(frag: RegisterUsernameFragment): RegisterUsernameView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: RegisterUsernameFragment): LifecycleOwner = frag
}
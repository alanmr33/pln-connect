package com.pln.intranet.authentication.onboarding.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.onboarding.presentation.OnBoardingView
import com.pln.intranet.authentication.onboarding.ui.OnBoardingFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class OnBoardingFragmentModule {

    @Provides
    @PerFragment
    fun onBoardingView(frag: OnBoardingFragment): OnBoardingView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: OnBoardingFragment): LifecycleOwner = frag
}
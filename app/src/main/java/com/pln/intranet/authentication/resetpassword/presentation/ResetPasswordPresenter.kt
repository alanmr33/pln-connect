package com.pln.intranet.authentication.resetpassword.presentation

import com.pln.intranet.authentication.presentation.AuthenticationNavigator
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.server.domain.GetConnectingServerInteractor
import com.pln.intranet.server.infraestructure.RocketChatClientFactory
import com.pln.intranet.util.extension.launchUI
import com.pln.intranet.util.retryIO
import chat.rocket.common.RocketChatException
import chat.rocket.common.RocketChatInvalidResponseException
import chat.rocket.common.util.ifNull
import chat.rocket.core.RocketChatClient
import chat.rocket.core.internal.rest.forgotPassword
import com.pln.intranet.SERVER_ROCKET_CHAT
import javax.inject.Inject

class ResetPasswordPresenter @Inject constructor(
    private val view: ResetPasswordView,
    private val strategy: CancelStrategy,
    private val navigator: AuthenticationNavigator,
    factory: RocketChatClientFactory,
    serverInteractor: GetConnectingServerInteractor
) {
    private val currentServer = serverInteractor.get()!!
    private val client: RocketChatClient = factory.create(currentServer)

    fun resetPassword(email: String) {
        launchUI(strategy) {
            view.showLoading()
            try {
                retryIO("forgotPassword(email = $email)") {
                    client.forgotPassword(email)
                }
                navigator.toPreviousView()
                view.emailSent()
            } catch (exception: RocketChatException) {
                if (exception is RocketChatInvalidResponseException) {
                    view.updateYourServerVersion()
                } else {
                    exception.message?.let {
                        view.showMessage(it)
                    }.ifNull {
                        view.showGenericErrorMessage()
                    }
                }
            } finally {
                view.hideLoading()
            }
        }
    }
    fun back(){
        navigator.toLogin(SERVER_ROCKET_CHAT)
    }
}
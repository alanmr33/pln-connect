package com.pln.intranet.authentication.registerusername.presentation

import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView

interface RegisterUsernameView : LoadingView, MessageView {

    /**
     * Enables the button to set the username if the user entered at least one character.
     */
    fun enableButtonUseThisUsername()

    /**
     * Disables the button to set the username when there is no character entered by the user.
     */
    fun disableButtonUseThisUsername()
}
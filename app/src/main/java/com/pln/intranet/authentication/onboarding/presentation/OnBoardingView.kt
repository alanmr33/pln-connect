package com.pln.intranet.authentication.onboarding.presentation

import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView

interface OnBoardingView : LoadingView, MessageView
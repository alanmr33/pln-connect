package com.pln.intranet.authentication.login.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.login.presentation.LoginView
import com.pln.intranet.authentication.login.ui.LoginFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class LoginFragmentModule {

    @Provides
    @PerFragment
    fun loginView(frag: LoginFragment): LoginView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: LoginFragment): LifecycleOwner = frag
}
package com.pln.intranet.authentication.presentation

interface AuthenticationView {
    fun showServerInput()
}
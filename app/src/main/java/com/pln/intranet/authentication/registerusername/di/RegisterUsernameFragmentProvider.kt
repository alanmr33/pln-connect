package com.pln.intranet.authentication.registerusername.di

import com.pln.intranet.authentication.registerusername.ui.RegisterUsernameFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class RegisterUsernameFragmentProvider {

    @ContributesAndroidInjector(modules = [RegisterUsernameFragmentModule::class])
    @PerFragment
    abstract fun provideRegisterUsernameFragment(): RegisterUsernameFragment
}
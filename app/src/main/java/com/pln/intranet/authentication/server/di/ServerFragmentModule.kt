package com.pln.intranet.authentication.server.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.server.presentation.ServerView
import com.pln.intranet.authentication.server.ui.ServerFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class ServerFragmentModule {

    @Provides
    @PerFragment
    fun serverView(frag: ServerFragment): ServerView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ServerFragment): LifecycleOwner = frag
}
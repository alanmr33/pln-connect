package com.pln.intranet.authentication.twofactor.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.twofactor.presentation.TwoFAView
import com.pln.intranet.authentication.twofactor.ui.TwoFAFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class TwoFAFragmentModule {

    @Provides
    @PerFragment
    fun loginView(frag: TwoFAFragment): TwoFAView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: TwoFAFragment): LifecycleOwner = frag
}

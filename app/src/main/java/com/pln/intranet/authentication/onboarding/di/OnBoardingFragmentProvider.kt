package com.pln.intranet.authentication.onboarding.di

import com.pln.intranet.authentication.onboarding.ui.OnBoardingFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class OnBoardingFragmentProvider {

    @ContributesAndroidInjector(modules = [OnBoardingFragmentModule::class])
    @PerFragment
    abstract fun provideOnBoardingFragment(): OnBoardingFragment
}
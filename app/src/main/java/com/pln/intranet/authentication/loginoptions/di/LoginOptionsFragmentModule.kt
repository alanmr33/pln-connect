package com.pln.intranet.authentication.loginoptions.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.loginoptions.presentation.LoginOptionsView
import com.pln.intranet.authentication.loginoptions.ui.LoginOptionsFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class LoginOptionsFragmentModule {

    @Provides
    @PerFragment
    fun loginOptionsView(frag: LoginOptionsFragment): LoginOptionsView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: LoginOptionsFragment): LifecycleOwner = frag
}
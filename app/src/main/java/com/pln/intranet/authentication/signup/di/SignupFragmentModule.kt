package com.pln.intranet.authentication.signup.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.authentication.signup.presentation.SignupView
import com.pln.intranet.authentication.signup.ui.SignupFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class SignupFragmentModule {

    @Provides
    @PerFragment
    fun signupView(frag: SignupFragment): SignupView = frag

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: SignupFragment): LifecycleOwner = frag
}
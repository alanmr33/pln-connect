package com.pln.intranet.authentication.resetpassword.di

import com.pln.intranet.authentication.resetpassword.ui.ResetPasswordFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ResetPasswordFragmentProvider {

    @ContributesAndroidInjector(modules = [ResetPasswordFragmentModule::class])
    @PerFragment
    abstract fun provideResetPasswordFragment(): ResetPasswordFragment
}
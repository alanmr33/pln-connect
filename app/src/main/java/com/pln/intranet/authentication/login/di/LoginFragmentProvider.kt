package com.pln.intranet.authentication.login.di

import com.pln.intranet.authentication.login.ui.LoginFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class LoginFragmentProvider {

    @ContributesAndroidInjector(modules = [LoginFragmentModule::class])
    @PerFragment
    abstract fun provideLoginFragment(): LoginFragment
}
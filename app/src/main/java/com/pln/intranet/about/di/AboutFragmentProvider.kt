package com.pln.intranet.about.di

import com.pln.intranet.about.ui.AboutFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AboutFragmentProvider {

    @ContributesAndroidInjector()
    abstract fun provideAboutFragment(): AboutFragment
}

package com.pln.intranet.chatdetails.presentation

import com.pln.intranet.chatdetails.domain.ChatDetails
import com.pln.intranet.core.behaviours.MessageView

interface ChatDetailsView: MessageView {
    fun displayDetails(room: ChatDetails)
}
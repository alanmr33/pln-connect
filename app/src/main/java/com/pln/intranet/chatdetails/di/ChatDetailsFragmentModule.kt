package com.pln.intranet.chatdetails.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.chatdetails.presentation.ChatDetailsView
import com.pln.intranet.chatdetails.ui.ChatDetailsFragment
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.db.ChatRoomDao
import com.pln.intranet.db.DatabaseManager
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.experimental.Job

@Module
class ChatDetailsFragmentModule {

    @Provides
    @PerFragment
    fun provideJob() = Job()

    @Provides
    @PerFragment
    fun chatDetailsView(frag: ChatDetailsFragment): ChatDetailsView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideChatRoomDao(manager: DatabaseManager): ChatRoomDao = manager.chatRoomDao()

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ChatDetailsFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}
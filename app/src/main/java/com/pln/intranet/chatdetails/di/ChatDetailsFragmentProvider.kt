package com.pln.intranet.chatdetails.di

import com.pln.intranet.chatdetails.ui.ChatDetailsFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ChatDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = [ChatDetailsFragmentModule::class])
    @PerFragment
    abstract fun providesChatDetailsFragment(): ChatDetailsFragment
}
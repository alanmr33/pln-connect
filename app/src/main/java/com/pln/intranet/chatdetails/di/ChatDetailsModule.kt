package com.pln.intranet.chatdetails.di

import com.pln.intranet.chatdetails.presentation.ChatDetailsNavigator
import com.pln.intranet.chatdetails.ui.ChatDetailsActivity
import com.pln.intranet.dagger.scope.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ChatDetailsModule {

    @Provides
    @PerActivity
    fun providesNavigator(activity: ChatDetailsActivity) = ChatDetailsNavigator(activity)
}
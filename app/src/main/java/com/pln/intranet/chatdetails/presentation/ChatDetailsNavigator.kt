package com.pln.intranet.chatdetails.presentation

import com.pln.intranet.R
import com.pln.intranet.chatdetails.ui.ChatDetailsActivity
import com.pln.intranet.favoritemessages.ui.TAG_FAVORITE_MESSAGES_FRAGMENT
import com.pln.intranet.files.ui.TAG_FILES_FRAGMENT
import com.pln.intranet.members.ui.TAG_MEMBERS_FRAGMENT
import com.pln.intranet.mentions.ui.TAG_MENTIONS_FRAGMENT
import com.pln.intranet.pinnedmessages.ui.TAG_PINNED_MESSAGES_FRAGMENT
import com.pln.intranet.util.extensions.addFragmentBackStack

class ChatDetailsNavigator(internal val activity: ChatDetailsActivity) {

    fun toMembersList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_MEMBERS_FRAGMENT, R.id.fragment_container) {
            com.pln.intranet.members.ui.newInstance(chatRoomId)
        }
    }

    fun toMentions(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_MENTIONS_FRAGMENT, R.id.fragment_container) {
            com.pln.intranet.mentions.ui.newInstance(chatRoomId)
        }
    }

    fun toPinnedMessageList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_PINNED_MESSAGES_FRAGMENT, R.id.fragment_container) {
            com.pln.intranet.pinnedmessages.ui.newInstance(chatRoomId)
        }
    }

    fun toFavoriteMessageList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_FAVORITE_MESSAGES_FRAGMENT, R.id.fragment_container) {
            com.pln.intranet.favoritemessages.ui.newInstance(chatRoomId)
        }
    }

    fun toFileList(chatRoomId: String) {
        activity.addFragmentBackStack(TAG_FILES_FRAGMENT, R.id.fragment_container) {
            com.pln.intranet.files.ui.newInstance(chatRoomId)
        }
    }
}

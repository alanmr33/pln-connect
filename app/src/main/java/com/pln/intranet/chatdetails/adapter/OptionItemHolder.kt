package com.pln.intranet.chatdetails.adapter

import com.pln.intranet.chatdetails.domain.Option
import com.pln.intranet.chatrooms.adapter.ItemHolder

data class OptionItemHolder(override val data: Option): ItemHolder<Option>
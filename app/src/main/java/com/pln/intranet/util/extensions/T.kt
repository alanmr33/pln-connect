package com.pln.intranet.util.extensions

val <T> T.exhaustive: T
    get() = this
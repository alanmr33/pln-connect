package com.pln.intranet.chatrooms.presentation

import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView

interface ChatRoomsView : LoadingView, MessageView {
    fun showLoadingRoom(name: CharSequence)

    fun hideLoadingRoom()
}
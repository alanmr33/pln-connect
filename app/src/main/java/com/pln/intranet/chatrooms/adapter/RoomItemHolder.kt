package com.pln.intranet.chatrooms.adapter

import com.pln.intranet.chatrooms.adapter.model.RoomUiModel

data class RoomItemHolder(override val data: RoomUiModel) : ItemHolder<RoomUiModel>
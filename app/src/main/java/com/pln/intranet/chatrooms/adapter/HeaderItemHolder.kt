package com.pln.intranet.chatrooms.adapter

data class HeaderItemHolder(override val data: String) : ItemHolder<String>
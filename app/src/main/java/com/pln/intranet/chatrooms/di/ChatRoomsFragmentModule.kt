package com.pln.intranet.chatrooms.di

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.chatrooms.adapter.RoomUiModelMapper
import com.pln.intranet.chatrooms.domain.FetchChatRoomsInteractor
import com.pln.intranet.chatrooms.presentation.ChatRoomsView
import com.pln.intranet.chatrooms.ui.ChatRoomsFragment
import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.db.ChatRoomDao
import com.pln.intranet.db.DatabaseManager
import com.pln.intranet.db.UserDao
import com.pln.intranet.server.domain.GetCurrentUserInteractor
import com.pln.intranet.server.domain.PermissionsInteractor
import com.pln.intranet.server.domain.PublicSettings
import com.pln.intranet.server.domain.SettingsRepository
import com.pln.intranet.server.domain.TokenRepository
import com.pln.intranet.server.infraestructure.ConnectionManager
import com.pln.intranet.server.infraestructure.ConnectionManagerFactory
import com.pln.intranet.server.infraestructure.RocketChatClientFactory
import chat.rocket.core.RocketChatClient
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ChatRoomsFragmentModule {

    @Provides
    @PerFragment
    fun chatRoomsView(frag: ChatRoomsFragment): ChatRoomsView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ChatRoomsFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideRocketChatClient(
        factory: RocketChatClientFactory,
        @Named("currentServer") currentServer: String
    ): RocketChatClient {
        return factory.create(currentServer)
    }

    @Provides
    @PerFragment
    fun provideChatRoomDao(manager: DatabaseManager): ChatRoomDao = manager.chatRoomDao()

    @Provides
    @PerFragment
    fun provideUserDao(manager: DatabaseManager): UserDao = manager.userDao()

    @Provides
    @PerFragment
    fun provideConnectionManager(
        factory: ConnectionManagerFactory,
        @Named("currentServer") currentServer: String
    ): ConnectionManager {
        return factory.create(currentServer)
    }

    @Provides
    @PerFragment
    fun provideFetchChatRoomsInteractor(
        client: RocketChatClient,
        dbManager: DatabaseManager
    ): FetchChatRoomsInteractor {
        return FetchChatRoomsInteractor(client, dbManager)
    }

    @Provides
    @PerFragment
    fun providePublicSettings(
        repository: SettingsRepository,
        @Named("currentServer") currentServer: String
    ): PublicSettings {
        return repository.get(currentServer)
    }

    @Provides
    @PerFragment
    fun provideRoomMapper(
        context: Application,
        repository: SettingsRepository,
        userInteractor: GetCurrentUserInteractor,
        @Named("currentServer") serverUrl: String,
        permissionsInteractor: PermissionsInteractor
    ): RoomUiModelMapper {
        return RoomUiModelMapper(context, repository.get(serverUrl), userInteractor, serverUrl, permissionsInteractor)
    }

    @Provides
    @PerFragment
    fun provideGetCurrentUserInteractor(
        tokenRepository: TokenRepository,
        @Named("currentServer") serverUrl: String,
        userDao: UserDao
    ): GetCurrentUserInteractor {
        return GetCurrentUserInteractor(tokenRepository, serverUrl, userDao)
    }
}
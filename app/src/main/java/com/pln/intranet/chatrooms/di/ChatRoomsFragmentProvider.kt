package com.pln.intranet.chatrooms.di

import com.pln.intranet.chatrooms.ui.ChatRoomsFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ChatRoomsFragmentProvider {

    @ContributesAndroidInjector(modules = [ChatRoomsFragmentModule::class])
    @PerFragment
    abstract fun provideChatRoomsFragment(): ChatRoomsFragment
}
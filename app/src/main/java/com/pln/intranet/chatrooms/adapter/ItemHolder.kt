package com.pln.intranet.chatrooms.adapter

interface ItemHolder<T> {
    val data: T
}
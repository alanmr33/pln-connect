package com.pln.intranet.chatinformation.presentation

import com.pln.intranet.chatroom.uimodel.UiModelMapper
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.server.domain.GetCurrentServerInteractor
import com.pln.intranet.server.infraestructure.ConnectionManagerFactory
import com.pln.intranet.util.extension.launchUI
import com.pln.intranet.util.retryIO
import chat.rocket.common.RocketChatException
import chat.rocket.core.internal.rest.getMessageReadReceipts
import timber.log.Timber
import javax.inject.Inject

class MessageInfoPresenter @Inject constructor(
    private val view: MessageInfoView,
    private val strategy: CancelStrategy,
    private val mapper: UiModelMapper,
    serverInteractor: GetCurrentServerInteractor,
    factory: ConnectionManagerFactory
) {

    private val currentServer = serverInteractor.get()!!
    private val manager = factory.create(currentServer)
    private val client = manager.client

    fun loadReadReceipts(messageId: String) {
        launchUI(strategy) {
            try {
                view.showLoading()
                val readReceipts = retryIO(description = "getMessageReadReceipts") {
                    client.getMessageReadReceipts(messageId = messageId).result
                }
                view.showReadReceipts(mapper.map(readReceipts))
            } catch (ex: RocketChatException) {
                Timber.e(ex)
                view.showGenericErrorMessage()
            } finally {
                view.hideLoading()
            }
        }
    }
}

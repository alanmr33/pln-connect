package com.pln.intranet.chatinformation.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.chatinformation.presentation.MessageInfoView
import com.pln.intranet.chatinformation.ui.MessageInfoFragment
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.experimental.Job

@Module
class MessageInfoFragmentModule {

    @Provides
    @PerFragment
    fun provideJob() = Job()

    @Provides
    @PerFragment
    fun messageInfoView(frag: MessageInfoFragment): MessageInfoView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: MessageInfoFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}

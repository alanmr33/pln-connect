package com.pln.intranet.chatinformation.presentation

import com.pln.intranet.chatinformation.viewmodel.ReadReceiptViewModel
import com.pln.intranet.core.behaviours.LoadingView

interface MessageInfoView : LoadingView {

    fun showGenericErrorMessage()

    fun showReadReceipts(messageReceipts: List<ReadReceiptViewModel>)
}

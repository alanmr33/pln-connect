package com.pln.intranet.chatroom.di

import com.pln.intranet.chatroom.presentation.ChatRoomNavigator
import com.pln.intranet.chatroom.ui.ChatRoomActivity
import com.pln.intranet.dagger.scope.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ChatRoomModule {
    @Provides
    @PerActivity
    fun provideChatRoomNavigator(activity: ChatRoomActivity) = ChatRoomNavigator(activity)
}
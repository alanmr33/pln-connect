package com.pln.intranet.chatroom.uimodel.suggestion

import com.pln.intranet.suggestions.model.SuggestionModel

class ChatRoomSuggestionUiModel(
    text: String,
    val fullName: String,
    val name: String,
    searchList: List<String>
) : SuggestionModel(text, searchList, false)

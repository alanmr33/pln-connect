package com.pln.intranet.chatroom.domain

data class MessageReply(val roomName: String, val permalink: String)
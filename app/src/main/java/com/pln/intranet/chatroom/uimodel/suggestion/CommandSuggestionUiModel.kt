package com.pln.intranet.chatroom.uimodel.suggestion

import com.pln.intranet.suggestions.model.SuggestionModel

class CommandSuggestionUiModel(
    text: String,
    val description: String,
    searchList: List<String>
) : SuggestionModel(text, searchList)
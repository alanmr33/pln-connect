package com.pln.intranet.chatroom.uimodel.suggestion

import com.pln.intranet.emoji.Emoji
import com.pln.intranet.suggestions.model.SuggestionModel

class EmojiSuggestionUiModel(
    text: String,
    pinned: Boolean = false,
    val emoji: Emoji,
    searchList: List<String>
) : SuggestionModel(text, searchList, pinned) {

    override fun toString(): String {
        return "EmojiSuggestionUiModel(text='$text', searchList='$searchList', pinned=$pinned)"
    }
}

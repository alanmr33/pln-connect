package com.pln.intranet.chatroom.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pln.intranet.R
import com.pln.intranet.chatroom.adapter.RoomSuggestionsAdapter.RoomSuggestionsViewHolder
import com.pln.intranet.chatroom.uimodel.suggestion.ChatRoomSuggestionUiModel
import com.pln.intranet.suggestions.model.SuggestionModel
import com.pln.intranet.suggestions.ui.BaseSuggestionViewHolder
import com.pln.intranet.suggestions.ui.SuggestionsAdapter

class RoomSuggestionsAdapter : SuggestionsAdapter<RoomSuggestionsViewHolder>("#") {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomSuggestionsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.suggestion_room_item, parent,
                false)
        return RoomSuggestionsViewHolder(view)
    }

    class RoomSuggestionsViewHolder(view: View) : BaseSuggestionViewHolder(view) {

        override fun bind(item: SuggestionModel, itemClickListener: SuggestionsAdapter.ItemClickListener?) {
            item as ChatRoomSuggestionUiModel
            with(itemView) {
                val fullname = itemView.findViewById<TextView>(R.id.text_fullname)
                val name = itemView.findViewById<TextView>(R.id.text_name)
                name.text = item.name
                fullname.text = item.fullName
                setOnClickListener {
                    itemClickListener?.onClick(item)
                }
            }
        }
    }
}
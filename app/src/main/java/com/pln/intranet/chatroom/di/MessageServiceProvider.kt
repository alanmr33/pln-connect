package com.pln.intranet.chatroom.di

import com.pln.intranet.chatroom.service.MessageService
import com.pln.intranet.dagger.module.AppModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class MessageServiceProvider {
    @ContributesAndroidInjector(modules = [AppModule::class])
    abstract fun provideMessageService(): MessageService
}
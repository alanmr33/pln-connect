package com.pln.intranet.chatroom.presentation

import com.pln.intranet.R
import com.pln.intranet.chatdetails.ui.chatDetailsIntent
import com.pln.intranet.chatinformation.ui.messageInformationIntent
import com.pln.intranet.chatroom.ui.ChatRoomActivity
import com.pln.intranet.chatroom.ui.chatRoomIntent
import com.pln.intranet.server.ui.changeServerIntent

class ChatRoomNavigator(internal val activity: ChatRoomActivity) {
    fun toChatDetails(
        chatRoomId: String,
        chatRoomType: String,
        isChatRoomSubscribed: Boolean,
        isMenuDisabled: Boolean
    ) {
        activity.startActivity(
            activity.chatDetailsIntent(
                chatRoomId,
                chatRoomType,
                isChatRoomSubscribed,
                isMenuDisabled
            )
        )
    }

    fun toNewServer() {
        activity.startActivity(activity.changeServerIntent())
        activity.finish()
    }

    fun toDirectMessage(
        chatRoomId: String,
        chatRoomName: String,
        chatRoomType: String,
        isChatRoomReadOnly: Boolean,
        chatRoomLastSeen: Long,
        isChatRoomSubscribed: Boolean,
        isChatRoomCreator: Boolean,
        isChatRoomFavorite: Boolean,
        chatRoomMessage: String
    ) {
        activity.startActivity(
            activity.chatRoomIntent(
                chatRoomId,
                chatRoomName,
                chatRoomType,
                isChatRoomReadOnly,
                chatRoomLastSeen,
                isChatRoomSubscribed,
                isChatRoomCreator,
                isChatRoomFavorite,
                chatRoomMessage
            )
        )
        activity.overridePendingTransition(R.anim.open_enter, R.anim.open_exit)
    }

    fun toMessageInformation(messageId: String) {
        activity.startActivity(activity.messageInformationIntent(messageId = messageId))
        activity.overridePendingTransition(R.anim.open_enter, R.anim.open_exit)
    }
}

package com.pln.intranet.files.uimodel

import com.pln.intranet.server.domain.GetCurrentServerInteractor
import com.pln.intranet.server.domain.GetSettingsInteractor
import com.pln.intranet.server.domain.TokenRepository
import com.pln.intranet.server.domain.baseUrl
import chat.rocket.core.model.Value
import chat.rocket.core.model.attachment.GenericAttachment
import javax.inject.Inject

class FileUiModelMapper @Inject constructor(
    serverInteractor: GetCurrentServerInteractor,
    getSettingsInteractor: GetSettingsInteractor,
    private val tokenRepository: TokenRepository
) {
    private var settings: Map<String, Value<Any>> =
        getSettingsInteractor.get(serverInteractor.get()!!)
    private val baseUrl = settings.baseUrl()

    fun mapToUiModelList(fileList: List<GenericAttachment>): List<FileUiModel> {
        return fileList.map { FileUiModel(it, settings, tokenRepository, baseUrl) }
    }
}
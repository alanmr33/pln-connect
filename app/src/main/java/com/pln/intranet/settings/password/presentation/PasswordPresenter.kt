package com.pln.intranet.settings.password.presentation

import com.pln.intranet.analytics.AnalyticsManager
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.server.domain.GetCurrentServerInteractor
import com.pln.intranet.server.infraestructure.RocketChatClientFactory
import com.pln.intranet.util.extension.launchUI
import com.pln.intranet.util.retryIO
import chat.rocket.common.RocketChatException
import chat.rocket.core.RocketChatClient
import chat.rocket.core.internal.rest.me
import chat.rocket.core.internal.rest.updateProfile
import javax.inject.Inject

class PasswordPresenter @Inject constructor(
    private val view: PasswordView,
    private val strategy: CancelStrategy,
    private val analyticsManager: AnalyticsManager,
    serverInteractor: GetCurrentServerInteractor,
    factory: RocketChatClientFactory
) {
    private val serverUrl = serverInteractor.get()!!
    private val client: RocketChatClient = factory.create(serverUrl)

    fun updatePassword(password: String) {
        launchUI(strategy) {
            try {
                view.showLoading()

                val me = retryIO("me") { client.me() }
                retryIO("updateProfile(${me.id})") {
                    client.updateProfile(me.id, null, null, password, null)
                }

                analyticsManager.logResetPassword(true)
                view.showPasswordSuccessfullyUpdatedMessage()
            } catch (exception: RocketChatException) {
                analyticsManager.logResetPassword(false)
                view.showPasswordFailsUpdateMessage(exception.message)
            } finally {
                view.hideLoading()
            }
        }
    }
}
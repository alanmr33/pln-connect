package com.pln.intranet.settings.password.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.settings.password.presentation.PasswordView
import com.pln.intranet.settings.password.ui.PasswordFragment
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.experimental.Job

@Module
class PasswordFragmentModule {

    @Provides
    @PerFragment
    fun provideJob() = Job()

    @Provides
    @PerFragment
    fun passwordView(frag: PasswordFragment): PasswordView {
        return frag
    }

    @Provides
    @PerFragment
    fun settingsLifecycleOwner(frag: PasswordFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}

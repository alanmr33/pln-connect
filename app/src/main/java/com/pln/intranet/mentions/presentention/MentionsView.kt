package com.pln.intranet.mentions.presentention

import com.pln.intranet.chatroom.uimodel.BaseUiModel
import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView

interface MentionsView : MessageView, LoadingView {

    /**
     * Shows the list of mentions for the current room.
     *
     * @param mentions The list of mentions.
     */
    fun showMentions(mentions: List<BaseUiModel<*>>)
}
package com.pln.intranet.webview.adminpanel.di

import com.pln.intranet.webview.adminpanel.ui.AdminPanelWebViewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AdminPanelWebViewFragmentProvider {

    @ContributesAndroidInjector
    abstract fun provideAdminPanelWebViewFragment(): AdminPanelWebViewFragment
}
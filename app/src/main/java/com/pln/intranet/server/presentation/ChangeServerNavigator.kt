package com.pln.intranet.server.presentation

import android.content.Intent
import com.pln.intranet.authentication.ui.newServerIntent
import com.pln.intranet.main.ui.MainActivity
import com.pln.intranet.server.ui.ChangeServerActivity
import com.pln.intranet.server.ui.INTENT_CHAT_ROOM_ID

class ChangeServerNavigator (internal val activity: ChangeServerActivity) {

    fun toServerScreen() {
        activity.startActivity(activity.newServerIntent())
        activity.finish()
    }

    fun toChatRooms(chatRoomId: String? = null) {
        activity.startActivity(Intent(activity, MainActivity::class.java).also {
            it.putExtra(INTENT_CHAT_ROOM_ID, chatRoomId)
        })
        activity.finish()
    }

}
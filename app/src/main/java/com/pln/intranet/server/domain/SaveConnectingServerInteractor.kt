package com.pln.intranet.server.domain

import com.pln.intranet.dagger.qualifier.ForAuthentication
import javax.inject.Inject

class SaveConnectingServerInteractor @Inject constructor(
    @ForAuthentication private val repository: CurrentServerRepository
) {
    fun save(url: String) = repository.save(url)
}
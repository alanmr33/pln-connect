package com.pln.intranet.server.domain

interface AnalyticsTrackingRepository {
    fun save(isAnalyticsTrackingEnable: Boolean)
    fun get(): Boolean
}
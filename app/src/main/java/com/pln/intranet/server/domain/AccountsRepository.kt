package com.pln.intranet.server.domain

import com.pln.intranet.server.domain.model.Account

interface AccountsRepository {
    fun save(account: Account)
    fun load(): List<Account>
    fun remove(serverUrl: String)
}
package com.pln.intranet.server.domain

import com.pln.intranet.server.domain.model.BasicAuth
import javax.inject.Inject

class SaveBasicAuthInteractor @Inject constructor(val repository: BasicAuthRepository) {
    fun save(basicAuth: BasicAuth) = repository.save(basicAuth)
}

package com.pln.intranet.server.domain

import com.pln.intranet.server.domain.model.BasicAuth

interface BasicAuthRepository {
    fun save(basicAuth: BasicAuth)
    fun load(): List<BasicAuth>
}

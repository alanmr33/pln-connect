package com.pln.intranet.server.domain

import com.pln.intranet.db.UserDao
import com.pln.intranet.db.model.UserEntity

class GetCurrentUserInteractor(
    private val tokenRepository: TokenRepository,
    private val currentServer: String,
    private val userDao: UserDao
) {
    fun get(): UserEntity? {
        return tokenRepository.get(currentServer)?.let {
            userDao.getUser(it.userId)
        }
    }

}

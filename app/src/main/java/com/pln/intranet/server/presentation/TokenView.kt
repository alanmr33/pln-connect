package com.pln.intranet.server.presentation

interface TokenView {

    fun invalidateToken(token: String)
}
package com.pln.intranet.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pln.intranet.db.model.AttachmentActionEntity
import com.pln.intranet.db.model.AttachmentEntity
import com.pln.intranet.db.model.AttachmentFieldEntity
import com.pln.intranet.db.model.ChatRoomEntity
import com.pln.intranet.db.model.MessageChannels
import com.pln.intranet.db.model.MessageEntity
import com.pln.intranet.db.model.MessageFavoritesRelation
import com.pln.intranet.db.model.MessageMentionsRelation
import com.pln.intranet.db.model.MessagesSync
import com.pln.intranet.db.model.ReactionEntity
import com.pln.intranet.db.model.UrlEntity
import com.pln.intranet.db.model.UserEntity
import com.pln.intranet.emoji.internal.db.StringListConverter

@Database(
    entities = [
        UserEntity::class, ChatRoomEntity::class, MessageEntity::class,
        MessageFavoritesRelation::class, MessageMentionsRelation::class,
        MessageChannels::class, AttachmentEntity::class,
        AttachmentFieldEntity::class, AttachmentActionEntity::class, UrlEntity::class,
        ReactionEntity::class, MessagesSync::class
    ],
    version = 11,
    exportSchema = true
)
@TypeConverters(StringListConverter::class)
abstract class RCDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun chatRoomDao(): ChatRoomDao
    abstract fun messageDao(): MessageDao
}

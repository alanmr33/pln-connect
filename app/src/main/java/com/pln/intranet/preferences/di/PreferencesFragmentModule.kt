package com.pln.intranet.preferences.di

import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.preferences.presentation.PreferencesView
import com.pln.intranet.preferences.ui.PreferencesFragment
import dagger.Module
import dagger.Provides

@Module
class PreferencesFragmentModule {

    @Provides
    @PerFragment
    fun preferencesView(frag: PreferencesFragment): PreferencesView {
        return frag
    }
}
package com.pln.intranet.preferences.di

import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.preferences.ui.PreferencesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PreferencesFragmentProvider {

    @ContributesAndroidInjector(modules = [PreferencesFragmentModule::class])
    @PerFragment
    abstract fun providePreferencesFragment(): PreferencesFragment
}
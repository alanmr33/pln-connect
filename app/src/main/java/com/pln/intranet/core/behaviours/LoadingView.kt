package com.pln.intranet.core.behaviours

interface LoadingView {

    fun showLoading()

    fun hideLoading()
}
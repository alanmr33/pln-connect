package com.pln.intranet.profile.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.profile.presentation.ProfileView
import com.pln.intranet.profile.ui.ProfileFragment
import dagger.Module
import dagger.Provides

@Module
class ProfileFragmentModule {

    @Provides
    @PerFragment
    fun profileView(frag: ProfileFragment): ProfileView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: ProfileFragment): LifecycleOwner {
        return frag
    }
}
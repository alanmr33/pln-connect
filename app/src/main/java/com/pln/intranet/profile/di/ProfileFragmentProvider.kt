package com.pln.intranet.profile.di

import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.profile.ui.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProfileFragmentProvider {

    @ContributesAndroidInjector(modules = [ProfileFragmentModule::class])
    @PerFragment
    abstract fun provideProfileFragment(): ProfileFragment
}
package com.pln.intranet.profile.presentation

import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView
import com.pln.intranet.server.presentation.TokenView

interface ProfileView : TokenView, LoadingView, MessageView {

    /**
     * Shows the user profile.
     *
     * @param avatarUrl The user avatar URL.
     * @param name The user display name.
     * @param username The user username.
     * @param email The user email.
     */
    fun showProfile(avatarUrl: String, name: String, username: String, email: String?)

    /**
     * Reloads the user avatar (after successfully updating it).
     *
     * @param avatarUrl The user avatar URL.
     */
    fun reloadUserAvatar(avatarUrl: String)

    /**
     * Shows a profile update successfully message
     */
    fun showProfileUpdateSuccessfullyMessage()
}
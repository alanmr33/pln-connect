package com.pln.intranet.members.presentation

import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView
import com.pln.intranet.members.uimodel.MemberUiModel

interface MembersView: LoadingView, MessageView {

    /**
     * Shows a list of members of a room.
     *
     * @param dataSet The data set to show.
     * @param total The total number of members.
     */
    fun showMembers(dataSet: List<MemberUiModel>, total: Long)
}
package com.pln.intranet.members.di

import com.pln.intranet.members.ui.MembersFragment
import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.members.ui.MemberBottomSheetFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MembersFragmentProvider {

    @ContributesAndroidInjector(modules = [MembersFragmentModule::class])
    @PerFragment
    abstract fun provideMembersFragment(): MembersFragment

    @ContributesAndroidInjector()
    @PerFragment
    abstract fun provideMemberBottomSheetFragment(): MemberBottomSheetFragment

}
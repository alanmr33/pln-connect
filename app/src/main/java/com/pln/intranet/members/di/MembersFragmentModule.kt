package com.pln.intranet.members.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.chatdetails.ui.ChatDetailsActivity
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.dagger.scope.PerFragment
import com.pln.intranet.members.presentation.MembersNavigator
import com.pln.intranet.members.presentation.MembersView
import com.pln.intranet.members.ui.MembersFragment
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.experimental.Job

@Module
class MembersFragmentModule {

    @Provides
    @PerFragment
    fun membersView(frag: MembersFragment): MembersView {
        return frag
    }

    @Provides
    @PerFragment
    fun provideChatRoomNavigator(activity: ChatDetailsActivity) = MembersNavigator(activity)

    @Provides
    @PerFragment
    fun provideJob() = Job()

    @Provides
    @PerFragment
    fun provideLifecycleOwner(frag: MembersFragment): LifecycleOwner {
        return frag
    }

    @Provides
    @PerFragment
    fun provideCancelStrategy(owner: LifecycleOwner, jobs: Job): CancelStrategy {
        return CancelStrategy(owner, jobs)
    }
}
package com.pln.intranet.members.presentation

import com.pln.intranet.chatdetails.ui.ChatDetailsActivity
import com.pln.intranet.userdetails.ui.userDetailsIntent

class MembersNavigator(internal val activity: ChatDetailsActivity) {

    fun toMemberDetails(userId: String) {
        activity.apply {
            startActivity(this.userDetailsIntent(userId, ""))
        }
    }
}

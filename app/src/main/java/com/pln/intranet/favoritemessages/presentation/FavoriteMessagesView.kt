package com.pln.intranet.favoritemessages.presentation

import com.pln.intranet.chatroom.uimodel.BaseUiModel
import com.pln.intranet.core.behaviours.LoadingView
import com.pln.intranet.core.behaviours.MessageView

interface FavoriteMessagesView : MessageView, LoadingView {

    /**
     * Shows the list of favorite messages for the current room.
     *
     * @param favoriteMessages The list of favorite messages to show.
     */
    fun showFavoriteMessages(favoriteMessages: List<BaseUiModel<*>>)
}
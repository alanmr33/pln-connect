package com.pln.intranet.main.presentation

import com.pln.intranet.authentication.server.presentation.VersionCheckView
import com.pln.intranet.core.behaviours.MessageView
import com.pln.intranet.main.uimodel.NavHeaderUiModel
import com.pln.intranet.server.domain.model.Account
import com.pln.intranet.server.presentation.TokenView
import chat.rocket.common.model.UserStatus

interface MainView : MessageView, VersionCheckView, TokenView {

    /**
     * Shows the current user status.
     *
     * @see [UserStatus]
     */
    fun showUserStatus(userStatus: UserStatus)

    /**
     * Setups the user account info (displayed in the nav. header)
     *
     * @param uiModel The [NavHeaderUiModel].
     */
    fun setupUserAccountInfo(uiModel: NavHeaderUiModel)

    /**
     * Setups the server account list.
     *
     * @param serverAccountList The list of server accounts.
     */
    fun setupServerAccountList(serverAccountList: List<Account>)

    fun closeServerSelection()

    fun showProgress()

    fun hideProgress()
}
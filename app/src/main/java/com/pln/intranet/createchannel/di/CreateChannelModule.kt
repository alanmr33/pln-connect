package com.pln.intranet.createchannel.di

import androidx.lifecycle.LifecycleOwner
import com.pln.intranet.createchannel.presentation.CreateChannelView
import com.pln.intranet.createchannel.ui.CreateChannelFragment
import com.pln.intranet.dagger.scope.PerFragment
import dagger.Module
import dagger.Provides

@Module
class CreateChannelModule {

    @Provides
    @PerFragment
    fun createChannelView(fragment: CreateChannelFragment): CreateChannelView {
        return fragment
    }

    @Provides
    @PerFragment
    fun provideLifecycleOwner(fragment: CreateChannelFragment): LifecycleOwner {
        return fragment
    }
}
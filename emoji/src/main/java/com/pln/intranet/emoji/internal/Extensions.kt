package com.pln.intranet.emoji.internal

import com.pln.intranet.emoji.Emoji

fun Emoji.isCustom(): Boolean = this.url != null

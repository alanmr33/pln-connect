package com.pln.intranet.draw.main.presenter

import android.graphics.Bitmap
import com.pln.intranet.core.lifecycle.CancelStrategy
import com.pln.intranet.util.extension.compressImageAndGetByteArray
import com.pln.intranet.util.extension.launchUI
import javax.inject.Inject

class DrawPresenter @Inject constructor(
    private val view: DrawView,
    private val strategy: CancelStrategy
) {

    fun processDrawingImage(bitmap: Bitmap) {
        launchUI(strategy) {
            val byteArray = bitmap.compressImageAndGetByteArray("image/png")
            if (byteArray != null) {
                view.sendByteArray(byteArray)
            } else {
                view.showWrongProcessingMessage()
            }
        }
    }
}
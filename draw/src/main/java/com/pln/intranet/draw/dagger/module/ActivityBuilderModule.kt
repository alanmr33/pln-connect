package com.pln.intranet.draw.dagger.module

import com.pln.intranet.draw.main.di.DrawModule
import com.pln.intranet.draw.main.ui.DrawingActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = [DrawModule::class])
    abstract fun contributeDrawingActivityInjector(): DrawingActivity
}
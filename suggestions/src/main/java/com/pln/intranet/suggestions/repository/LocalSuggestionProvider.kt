package com.pln.intranet.suggestions.repository

interface LocalSuggestionProvider {
    fun find(prefix: String)
}